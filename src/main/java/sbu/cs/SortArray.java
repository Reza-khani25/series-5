package sbu.cs;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {

        for(int i = 0 ; i<size ; i++)
        {
            int min = arr[i];
            int minIndex = i;

            for(int j = i+1 ; j<size ; j++)
            {
                if(arr[j]<min)
                {
                    min = arr[j] ;
                    minIndex = j;
                }
            }

            int temp = arr[i];
            arr[i] = arr[minIndex] ;
            arr[minIndex] = temp;


        }

        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {

        for (int i = 1; i < size; i++)
        {
            int key = arr[i] ;
            int j = i-1;

            while(j>=0 && arr[j]>key )
            {
                arr[j+1] = arr[j];
                j--;
            }

            arr[j+1] = key;
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {

        if(size<=1)
            return arr ;

        int[] first = new int[size/2] ;
        int[] second= new int[size - first.length];


        for(int i=0 ; i<size/2 ; i++)
            first[i] = arr[i] ;

        for(int i = size/2  ; i<size ; i++)
            second[i-size/2] = arr[i] ;

       first = mergeSort(first , first.length) ;
       second = mergeSort(second , second.length) ;


        return merge(first , second) ;

    }

    public static int[] merge(int[] first , int[] second){

        int[] merged = new int[first.length + second.length] ;
        int i=0 , f=0 , s=0 ;

        while(f<first.length && s<second.length)
        {
            if(first[f]<=second[s]) {
                merged[i] = first[f] ;
                f++;
            }
            else {
                merged[i] = second[s];
                s++;
            }

            i++;
        }

        while(s<second.length)
        {
            merged[i] = second[s] ;
            s++;
            i++;
        }
        while ((f<first.length))
        {
            merged[i] = first[f] ;
            f++;
            i++;
        }


        return merged;
    }

    public static void print(int[] arr)
    {
        System.out.println("\nnew");
        for (int i : arr)
            System.out.print(i + " ");
        System.out.println("end");
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {

        int l = 0 , r=arr.length-1 ;

        while(l<=r)
        {
            int middleIndex = (l+r)/2;

            if(arr[middleIndex]==value)
                return middleIndex;

            else if(arr[middleIndex]<value)
                l = middleIndex+1;

            else
                r = middleIndex-1;

        }

        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        return binarySearchRecursive(arr , 0 , arr.length-1 , value);
    }

    public int binarySearchRecursive(int[] arr , int l , int r , int value)
    {
        int m = (l+(r-1))/2;

        if(l<=r)
        {
            if(arr[m]==value)
                return m ;

            if(arr[m]<value)
              return   binarySearchRecursive(arr , m+1  , r , value);

            else
              return    binarySearchRecursive(arr , l , m-1 , value);
        }

        return -1;
    }
}
